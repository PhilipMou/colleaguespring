package com.nine.colleaguespring.controller;

import com.nine.colleaguespring.model.Colleague;
import com.nine.colleaguespring.service.ColleagueServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/colleagues")
public class ColleaguesController {

    private ColleagueServiceImpl colleagueService;

    @Autowired
    public ColleaguesController(ColleagueServiceImpl colleagueService){
        this.colleagueService=colleagueService;
    }

    @GetMapping
    public List<Colleague> list(){
        return colleagueService.list();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Colleague get(@PathVariable Long id){
        return colleagueService.get(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Colleague create(@RequestBody final Colleague colleague){
        return colleagueService.create(colleague);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable Long id){
        //Also need to check for children records before deleting.
        colleagueService.delete(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    //because this is a PUT, we expect all attributes to be passed in.
    //TODO: Add validation that all attributes are passed in, otherwise return a 400 bad payload
    public Colleague update(@PathVariable Long id, @RequestBody Colleague colleague) {

        return colleagueService.update(id, colleague);
    }
}
