package com.nine.colleaguespring.controller;

import com.nine.colleaguespring.model.Department;
import com.nine.colleaguespring.service.DepartmentServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/departments")
public class DepartmentController {

    @Autowired
    private DepartmentServiceImpl departmentService;

    @GetMapping
    public List<Department> list(){
        return departmentService.list();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Department get(@PathVariable Long id){
        return departmentService.get(id);
    }

    @PostMapping
    public Department create(@RequestBody final Department department){
        return departmentService.create(department);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable Long id){
        //Also need to check for children records before deleting.
        departmentService.delete(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    //because this is a PUT, we expect all attributes to be passed in.
    //TODO: Add validation that all attributes are passed in, otherwise return a 400 bad payload
    public Department update(@PathVariable Long id, @RequestBody Department department) {
        Department existingDepartment = departmentService.get(id);
        BeanUtils.copyProperties(department, existingDepartment, "session_id");
        return departmentService.update(id, existingDepartment);
    }
}
