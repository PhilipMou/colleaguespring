package com.nine.colleaguespring.service;

import com.nine.colleaguespring.model.Colleague;
import com.nine.colleaguespring.repository.ColleagueRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class ColleagueServiceImpl{

    private ColleagueRepository colleagueRepository;

    public ColleagueServiceImpl(ColleagueRepository colleagueRepository){
        this.colleagueRepository=colleagueRepository;
    }

    @GetMapping
    public List<Colleague> list(){
        return colleagueRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Colleague get(@PathVariable Long id){
        return colleagueRepository.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Colleague create(@RequestBody final Colleague colleague){
        return colleagueRepository.saveAndFlush(colleague);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable Long id){
        //Also need to check for children records before deleting.
        colleagueRepository.deleteById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    //because this is a PUT, we expect all attributes to be passed in.
    //TODO: Add validation that all attributes are passed in, otherwise return a 400 bad payload
    public Colleague update(@PathVariable Long id, @RequestBody Colleague colleague) {
        Colleague existingColleague = colleagueRepository.getById(id);
        BeanUtils.copyProperties(colleague, existingColleague, "session_id");
        return colleagueRepository.saveAndFlush(existingColleague);
    }

}
