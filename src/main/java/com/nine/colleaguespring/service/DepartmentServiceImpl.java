package com.nine.colleaguespring.service;

import com.nine.colleaguespring.model.Department;
import com.nine.colleaguespring.repository.DepartmentRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class DepartmentServiceImpl {

    private DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    @GetMapping
    public List<Department> list(){
        return departmentRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Department get(@PathVariable Long id){
        return departmentRepository.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Department create(@RequestBody final Department department){
        return departmentRepository.saveAndFlush(department);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable Long id){
        //Also need to check for children records before deleting.
        departmentRepository.deleteById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    //because this is a PUT, we expect all attributes to be passed in.
    //TODO: Add validation that all attributes are passed in, otherwise return a 400 bad payload
    public Department update(@PathVariable Long id, @RequestBody Department department) {
        Department existingDepartment = departmentRepository.getById(id);
        BeanUtils.copyProperties(department, existingDepartment, "session_id");
        return departmentRepository.saveAndFlush(existingDepartment);
    }
}
