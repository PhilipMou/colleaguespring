package com.nine.colleaguespring.repository;

import com.nine.colleaguespring.model.Colleague;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ColleagueRepository extends JpaRepository<Colleague, Long> {

}
