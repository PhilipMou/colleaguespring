package com.nine.colleaguespring.repository;

import com.nine.colleaguespring.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
