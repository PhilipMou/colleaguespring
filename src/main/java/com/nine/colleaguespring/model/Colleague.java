package com.nine.colleaguespring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity(name = "colleagues")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Colleague {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long colleague_id;
    @NotNull
    private String firstname;
    @NotNull
    private String lastname;
    @NotNull
    private String colleague_role;

    @ManyToMany(mappedBy = "colleagues")
    @JsonIgnore
    private List<Department> departments;


    public Colleague (){

    }

    public long getColleague_id() {
        return colleague_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getColleague_role() {
        return colleague_role;
    }

    public void setColleague_id(long colleague_id) {
        this.colleague_id = colleague_id;
    }

    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }

    public void setLastname(String lastName) {
        this.lastname = lastName;
    }

    public void setColleague_role(String colleague_role) {
        this.colleague_role = colleague_role;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}
