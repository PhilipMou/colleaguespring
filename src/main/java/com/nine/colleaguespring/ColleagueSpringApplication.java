package com.nine.colleaguespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//T8cdehFBy7SwmyDsG8r3 app password
@SpringBootApplication
public class ColleagueSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColleagueSpringApplication.class, args);
	}

}
